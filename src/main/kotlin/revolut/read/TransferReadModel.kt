package revolut.read

import revolut.model.Transfer

/**
 * Simple read model that just returns current state for all transfers
 */
interface TransferReadModel {
    fun allTransfers(): List<Transfer>
}
