package revolut.model

interface Account {
    val id: String
    val balance: Long
}
